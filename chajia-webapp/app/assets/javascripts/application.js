// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//




/**BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) **/



/** BEGIN CORE PLUGINS**/ 

//= require 'jquery'
//= require 'jquery-migrate-1.2.1.min.js'
//= require jquery_ujs
//= require turbolinks

/** IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip **/

//= require 'bootstrap/js/bootstrap.min.js'
//= require 'bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js'
//= require 'jquery-slimscroll/jquery.slimscroll.min.js'
//= require 'jquery.blockui.min.js'
//= require 'jquery.cookie.min.js'
//= require 'uniform/jquery.uniform.min.js'

/** END CORE PLUGINS**/ 



/** BEGIN PAGE LEVEL PLUGINS **/

//= require 'flot/jquery.flot.js'
//= require 'flot/jquery.flot.resize.js'		
//= require 'flot/jquery.flot.pie.js'	
//= require 'flot/jquery.flot.stack.js'	
//= require 'flot/jquery.flot.crosshair.js'	
//= require 'flot/jquery.flot.time.js'
//= require 'elastislide/js/modernizr.custom.17475.js'
//= require 'elastislide/js/jquery.elastislide.js'
/** END PAGE LEVEL PLUGINS **/


/** BEGIN PAGE LEVEL SCRIPTS **/

//= require 'app.js'
//= require 'charts.js'	

/** END PAGE LEVEL SCRIPTS **/

jQuery(document).ready(function() {
	// initiate layout and plugins
	App.init();
	// Charts.init();
	// Charts.initCharts();
	// $('#carousel').elastislide();
});

/**END JAVASCRIPTS **/


