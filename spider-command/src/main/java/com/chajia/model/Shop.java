package com.chajia.model;

import com.chajia.util.DateUtil;

import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-1-19
 * Time: 上午10:17
 * To change this template use File | Settings | File Templates.
 */
public class Shop {
    public  final  static  String TABLE="shops";
    private Integer id;
    private String sUrl;
    private String shopName;
    private boolean valid=true;
    private Date created_at;
    private Date updated_at;

    public Shop(String sUrl,String shopName) {
        this.sUrl=sUrl;
        this.shopName=shopName;
        this.created_at=new Date(DateUtil.getDate());
        this.updated_at=created_at;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getsUrl() {
        return sUrl;
    }

    public void setsUrl(String sUrl) {
        this.sUrl = sUrl;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }



    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }


    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

}
